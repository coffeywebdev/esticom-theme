/* define dependencies */
var {gulp, src, dest, watch, series, parallel} = require('gulp');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var minify = require('gulp-cssnano');
var prefix = require('gulp-autoprefixer');
var devip = require('dev-ip');
var bsync = require('browser-sync').create();

var paths = {
  sass: './css/scss/style.scss',
  input: ['css/**/*.scss'],
  php: ['*.php'],
  output: 'css/'

}

/* browser-sync */
// gulp.task('browser-sync', function() {
//   var ip = devip()[0];
//   browserSync.init({
//     proxy: 'dev.esticom.com',
//     host: ip,
//     port: 3000
//   });
// });

var browserSyncStart = function(done) {
  bsync.init({
    proxy: 'dev.esticom.com',
    port: 5000
  });
  done();
}

function browserSyncReload(done) {
  bsync.reload();
  done();
}



var buildStyles = function (done) {

  return src(paths.sass)
    .pipe(sass({
      outputStyle: 'expanded',
      sourceComments: true
    }))
    .pipe(prefix({
      browsers: ['last 2 version', '> 0.25%'],
      cascade: true,
      remove: true
    }))
      .pipe(dest(paths.output))
      .pipe(rename({suffix: '.min'}))
      .pipe(minify({
        discardComments: {
          removeAll: true
        }
      }))
      .pipe(dest(paths.output))
      .pipe(bsync.stream());
}

var watchSource = function(done) {
  watch(paths.input, series(exports.default))
  watch(paths.php, browserSyncReload)
  done()
}

exports.default = series(buildStyles)

exports.watch = series(browserSyncStart,watchSource)
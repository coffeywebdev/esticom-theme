<?php
/**
 * Template for Product page
 *
 * Template Name: Product page
 *
 * @package WordPress
 * @subpackage Esticom
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

get_header(); ?>

    <?php
    // Start the loop.
    while ( have_posts() ) : the_post();

      $image = wp_get_attachment_image_src(get_post_thumbnail_id($imageID), 'pagetitle-image');
      $header_background = $image[0];
      $page_title = get_the_title();
      $headline_copy = get_field('section_one_headline');
    ?>

    <!-- HEADER -->
    <div class="trade-page-header" data-uk-parallax="bgy: -200" style="background:url('/wp-content/themes/esticom/img/trade-pages/background-image.jpg');">

        <div class="pagetitle uk-vertical-align-middle">
          <div class="uk-container uk-container-center">
            <div class="uk-grid">
              <div class="uk-width-xlarge-1-2">
                <span class="small-title"><?= $page_title; ?></span>
                <h1 class="big-title"><?= $headline_copy; ?></h1>
                <div class="button-group uk-grid">
                  <div class="uk-width-xlarge-1-2 uk-width-small-1-2">
                    <a class="uk-button uk-button-large yellow-button">Try It Free</a>
                  </div>
                  <div class="uk-width-xlarge-1-2 uk-width-small-1-2">
                    <a class="uk-button uk-button-large transparent-button">See Pricing</a>
                  </div>
                </div>
              </div>
              <div class="uk-width-xlarge-1-2">
                <?php $sectionOne = get_field('section_one_video_image'); ?> <!-- todo: upload laptop light to custom field -->
                <?php $videoPlay = get_field('section_one_video_play');?>
                <a href="https://www.youtube.com/watch?v=<?php the_field('section_one_video_link'); ?>" class="video-btn uk-overlay" data-uk-lightbox>
                  <img src="/wp-content/themes/esticom/img/trade-pages/laptop-light-with-text.png" alt="<?= $page_title; ?>"/>
                </a>

              </div>
            </div> <!-- UK Grid -->
          </div>
        </div> <!-- Pagetitle -->

    </div> <!-- Page Header -->

    <!-- FEATURES -->
    <div class="features">
      <div class="uk-container uk-container-center">
        <div class="headlines">
          <span class="small-title">Product Features</span>
          <h2 class="big-title">Win More Projects in Less Time</h2>
        </div>
        <div class="uk-grid">
          <div class="uk-width-xlarge-1-3 uk-width-small-1-1">
            <div class="feature">
              <img src="/wp-content/themes/esticom/img/trade-pages/manage-effectively.svg"/>
              <span class="feature-title">Manage projects effectively with CRM Dashboard</span>
              <p>Organize projects by bid due date or project stage. Quickly delegate to available team members. View individual and team performance.</p>
            </div>
          </div>
          <div class="uk-width-xlarge-1-3 uk-width-small-1-1">
            <div class="feature">
              <img src="/wp-content/themes/esticom/img/trade-pages/integrated-takeoff.svg"/>
              <span class="feature-title">Integrated Takeoff & Estimating in a single package</span>
              <p>As you take off items from a set of plans, the estimate is already being created, using your preferred materials and vendor part numbers. No need to transfer data between paper plans or spreadsheets.</p>

            </div>
          </div>
          <div class="uk-width-xlarge-1-3 uk-width-small-1-1">
            <div class="feature">
              <img src="/wp-content/themes/esticom/img/trade-pages/save-template.svg"/>
              <span class="feature-title">Create and save project templates to speed your set up</span>
              <p>Use project templates with pre-built material lists, pricing and groups to streamline your set up process. All you have to do is upload plans and start counting/measuring.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Content Section #1 -->
    <div class="content background-light-gray powered-by-craftsmen">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-large-1-2">
                    <?php $sectionTwo = get_field('section_two_image'); ?>
                    <img src="<?php echo $sectionTwo; ?>" alt="Esticom" />
                  <p class="button-wrapper centered"><a href="<?php the_field('section_two_button_link'); ?>" class="uk-button uk-button-large yellow-button"><?php the_field('section_two_button'); ?></a></p>
                </div>
                <div class="uk-width-large-1-2">
                    <h2 class="big-title"><?php the_field('section_two_title'); ?></h2>
                    <p><?php the_field('section_two_content'); ?></p>
                </div>
            </div>
        </div>
    </div> <!-- Content -->

    <!-- Testimonials -->
    <div class="testimonial background-blue">
      <div class="uk-container uk-container-center uk-text-center">
        <img src="/wp-content/themes/esticom/img/trade-pages/quote-icon.svg"/>

        <div class="testimonial-wrapper" data-uk-slideshow="{autoplay:true}">
          <ul class="uk-slideshow">
            <?php
              $mypost = array( 'post_type' => 'testimonials', 'order' => 'ASC', 'posts_per_page' => -1 );
              $loop = new WP_Query( $mypost );
              while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <li class="testimonial-content">
                  <div class="testimonial">
                    <?php the_content(); ?>
                  </div>
                  <span class="author">— <?php the_title(); ?></span>
                </li>
              <?php endwhile; ?>
            <?php wp_reset_query(); ?>
          </ul> <!-- Slideshow -->
          <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
          <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
        </div>


      </div> <!-- UK Container -->
    </div>

    <!-- Content Section #2 -->
    <div class="content">
        <div class="uk-container uk-container-center">
          <div class="uk-grid">
            <div class="uk-width-large-1-2">
              <h2><?php the_field('section_three_title'); ?></h2>
              <p><?php the_field('section_three_content'); ?></p>
            </div>
            <div class="uk-width-large-1-2">
              <div class="section-three-img">
                <?php $sectionThree = get_field('section_three_image'); ?>
                <img src="<?php echo $sectionThree; ?>" alt="Esticom" />
              </div>
              <div class="button-group uk-grid">
                <div class="uk-width-xlarge-1-2 uk-width-small-1-2">
                  <a href="<?php the_field('section_three_button_link'); ?>" class="uk-button uk-button-large-custom yellow-button"><?php the_field('section_tree_button'); ?></a>
                </div>
                <div class="uk-width-xlarge-1-2 uk-width-small-1-2">
                  <a href="<?php the_field('section_three_demo_button_link'); ?>" class="uk-button uk-button-large-custom blue-transparent-button "><?php the_field('section_tree_demo_button'); ?></a>
                </div>
              </div>
            </div>
          </div>
        </div>
  </div> <!-- Content -->

    <div id="paper">
      <div class="uk-container uk-container-center">
          <div class="uk-grid uk-grid-small">
              <?php $paperImg = get_field('paper_image', 'option'); ?>
              <div class="uk-width-medium-2-10">
                <img src="<?php echo $paperImg;?>" />
              </div>
              <div class="uk-width-medium-6-10 uk-text-center">
                <h2><?php the_field('paper_title', 'option'); ?></h2>
              </div>
              <div class="uk-width-medium-2-10">
                <a href="<?php the_field('paper_button_link'); ?>" class="uk-button uk-button-large-custom yellow-button"><?php the_field('paper_button'); ?></a>
              </div>
          </div>
      </div>
  </div> <!-- White Paper -->

    <section class="pricing uk-text-center">
      <div class="uk-container uk-container-center">
          <h1 class="uk-text-center"><?php the_field('pricing_title', 'option'); ?></h1>
          <?php if( have_rows('pricing', 'option') ): ?>
              <div class="uk-grid features-list">
              <?php while ( have_rows('pricing', 'option') ) : the_row(); ?>
                  <div class="feature uk-text-center uk-width-medium-1-3">
                      <h2><?php the_sub_field('title', 'option'); ?></h2>
                      <p><?php the_sub_field('text', 'option'); ?></p>
                  </div>
              <?php endwhile; ?>
              </div> <!-- Box -->
          <?php endif; ?>
          <a href="<?php the_field('pricing_button_link', 'option'); ?>" class="uk-button uk-button-primary"><?php the_field('pricing_button', 'option'); ?></a>
      </div> <!-- UK Container -->
  </section> <!-- Trading Features -->

    <?php // End the loop.
    endwhile;
    ?>
    
<?php get_footer(); ?>
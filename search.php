<?php
/**
 * Search Template
 *
 * Displays search results
 *
 */
// Exit if accessed directly
if ( ! defined( 'WPINC' ) ) {
	die;
}
get_header(); ?>

<?php get_template_part( 'pagetitle-search' ); ?>

<div class="content">
    <div class="uk-container uk-container-center">
        <div class="content-box">        
        	<?php if ( have_posts() ) : ?>
				<div class="uk-grid">
					<div class="uk-width-medium-7-10">
						<?php while ( have_posts() ) : the_post(); ?>
							<div class="post">
								<?php if(has_post_thumbnail()) { ?>
								<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'post-image' ); ?></a>
								<?php } ?>
								<div class="post-inner">
									<span class="date"><?php the_date(); ?></span>
									<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
									<p><?php echo excerpt(30); ?>...</p>
									<a class="more" href="<?php the_permalink(); ?>">Read More</a>
								</div>
							</div> <!-- Post -->
						<?php endwhile; ?>				
						<?php wp_pagenavi(); ?>
					</div>
					<div class="uk-width-medium-3-10">
						<div class="sidebar">
							<?php get_sidebar( 'second' ); ?>
						</div> <!-- Sidebar -->
					</div>
				</div>
			<?php else : ?>                    
                <h1>Nothing founded for <?php echo get_search_query(); ?>, please try again:</h1>
                <?php get_search_form(); ?>                    
            <?php endif; ?>
        </div> <!-- Content Box -->
    </div> <!-- UK Container -->
</div> <!-- Content -->
    
<?php get_footer(); ?>
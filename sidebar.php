<?php
/**
 * Default Sidebar
 *
 * @package WordPress
 * @subpackage Esticom
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}
?>

<div class="footer-box" role="complementary" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
    <?php
    global $sidebars_widgets;
    $count = count($sidebars_widgets['footer-sidebar']);
    switch ( $count ) {
		case '1':
			$largeGrid = $mediumGrid = $smallGrid = '1-1';
			break;
		case '2':
			$largeGrid = $mediumGrid = $smallGrid = '1-2';
			break;
		case '3':
			$largeGrid = '1-3';
            $mediumGrid = $smallGrid = '1-2';
            break;
		case '4':
			$largeGrid = '1-4';
            $mediumGrid = '1-4';
            $smallGrid = '1-2';
			break;
		case '5':
			$largeGrid = '1-5';
            $mediumGrid = '1-3';
            $smallGrid = '1-2';
			break;
		default:
			$largeGrid = $mediumGrid = $smallGrid = '1-1';
			break;
	}    
    ?>
    <div class="uk-grid uk-grid-width-large-<?php echo $largeGrid; ?> uk-grid-width-medium-<?php echo $mediumGrid; ?> uk-grid-width-<?php echo $smallGrid; ?>" data-uk-grid-margin>
        <?php if ( ! dynamic_sidebar('footer-sidebar') ) :
            dynamic_sidebar('footer-sidebar');
        endif; // end sidebar widget area ?>    
    </div>
</div>
<?php
/**
 * Template Name: Home page
 *
 * The template for website home/landing page.
 *
 * @package WordPress
 * @subpackage Esticom
 */
get_header(); ?>

<?php
// Start the loop.
while ( have_posts() ) : the_post(); ?>
    
    <?php $heroBgImage = get_field('hero_bg_image'); ?>
    <?php $heroPanelImage = get_field('hero_image'); ?>
    <div id="hero" style="background:url(<?php echo $heroBgImage; ?>) top center no-repeat">
        <div class="hero-panel">
            <div class="uk-container uk-container-center">
                <div class="uk-grid uk-grid-large">
                    <div class="uk-width-xlarge-4-10 uk-width-large-1-2">
                        <div class="hero-intro">
                            <h1><?php the_field('hero_title'); ?></h1>
                            <p><?php the_field('hero_text'); ?></p>
                            <div class="work-email">
                                <?php echo do_shortcode('[ninja_form id=3] '); ?>
                            </div>
                        </div> <!-- Hero Intro -->
                    </div>
                </div>
                <div class="hero-panel-img">
                    <img src="<?php echo $heroPanelImage; ?>" />
                </div>
            </div>
        </div>
    </div> <!-- Hero -->

    <?php $more = get_field('header_block_bg_image'); ?>
    <div id="header-block" style="background:url(<?php echo $more; ?>) no-repeat top center; background-size: cover;">
        <div class="uk-container uk-container-center">
            <h2><?php the_field('header_block'); ?></h2>
        </div>
    </div> <!-- Content -->

    <div id="content" class="content">
        <div class="uk-container uk-container-center">
            <h1><?php the_field('home_content_title'); ?></h1>
            <?php the_field('home_content_text'); ?>
        </div>
    </div> <!-- Content -->

    <div id="ourApp">
        <div class="uk-container uk-container-center uk-text-center">
            <div class="uk-grid">
                <?php if( have_rows('app_box') ): ?>
                <?php while ( have_rows('app_box') ) : the_row(); ?>
                <div class="uk-width-medium-1-3">
                    <div class="app-box">
                        <?php $appImage = get_sub_field('app_box_image');?>
                        <img src="<?php echo $appImage['url']; ?>" alt="<?php echo $appImage['alt']; ?>"  />
                        <h3><?php the_sub_field('app_box_title'); ?></h3>
                        <p><?php the_sub_field('app_box_text'); ?></p>
                    </div> <!-- App Box -->
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div> <!-- UK Container -->
    </div> <!-- OurApp -->

    <?php $heroVideoThumb = get_field('video_bg_image'); ?>
    <?php if($heroVideoThumb) { ?>
    <div id="video">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-medium-1-2 uk-flex uk-flex-center uk-flex-middle">
                    <h2><?php the_field('video_title'); ?></h2>
                </div>
                <div class="uk-width-medium-1-2">
                    <div class="hero-img">
                        <?php $heroImage = get_field('video_play');?>
                        <a href="https://www.youtube.com/watch?v=<?php the_field('video_button_link'); ?>" class="video-btn uk-overlay" data-uk-lightbox>
                            <img src="<?php echo $heroVideoThumb; ?>" alt="Esticom" />
                            <figcaption class="uk-overlay-panel uk-flex uk-flex-center uk-flex-middle uk-text-center">
                                <div>
                                    <img src="<?php echo $heroImage; ?>" alt="Esticom" />
                                </div>
                            </figcaption>
                        </a>
                    </div> <!-- Hero Img -->
                </div>
            </div>
        </div> <!-- UK Container -->
    </div> <!-- Video -->
    <?php } ?>

    <div class="testimonials">
        <div class="uk-container uk-container-center uk-text-center">
            <div class="uk-slidenav-position" data-uk-slideshow="{autoplay:true}">
                <ul class="uk-slideshow">
                    <?php 
                    $mypost = array( 'post_type' => 'testimonials', 'order' => 'ASC', 'posts_per_page' => -1 );
                    $loop = new WP_Query( $mypost );
                    while ( $loop->have_posts() ) : $loop->the_post(); ?>
                        <li>
                            <?php the_content(); ?>
                            <h3><?php the_title(); ?></h3>
                        </li>
                    <?php endwhile; ?>
                    <?php wp_reset_query(); ?>
                </ul> <!-- Slideshow -->
                <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
                <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
            </div>
        </div> <!-- UK Container -->
    </div> <!-- Testimonials -->

<?php // End the loop.
endwhile;
?>


<?php get_footer(); ?>
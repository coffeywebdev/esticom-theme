<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Esticom
 * @since Esticom 1.0
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

get_header();

    $id = get_id_by_slug('support');
    $image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'pagetitle-image'); ?>
    <div id="page-header" class="uk-vertical-align" style="background:url(<?php echo $image[0]; ?>) center center no-repeat;">
        <div class="pagetitle uk-vertical-align-middle">
            <div class="uk-container uk-container-center">
                <div class="uk-grid">
                    <div class="uk-width-xlarge-1-1">
                        <h1>404</h1>
                    </div>
                    <div class="uk-width-xlarge-1-1">
                        <p><?php _e( 'Oops! That page can&rsquo;t be found.', 'esticom' ); ?></p>
                    </div>
                </div> <!-- UK Grid -->
            </div>
        </div> <!-- Pagetitle -->
    </div> <!-- Page Header -->
    <div class="content">
        <div class="uk-container uk-container-center">
            <div class="content-box">
                <p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'esticom' ); ?></p>
                <?php get_search_form(); ?>
            </div> <!-- Content Box -->
        </div> <!-- UK Container -->
    </div> <!-- Content -->
    
<?php get_footer(); ?>
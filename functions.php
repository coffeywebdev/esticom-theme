<?php
/**
 * Esticom theme functions and definitions
 *
 * @package WordPress
 * @subpackage Esticom
 */

function esticom_read_time() {
	$content = get_post_field( 'post_content', $post->ID );
	$word_count = str_word_count( strip_tags( $content ) );
	$readingtime = ceil($word_count / 200);
	return $readingtime;
}

/* remove sticky posts from the loop */
add_action('pre_get_posts', 'esticom_ignore_sticky');
function esticom_ignore_sticky($query)
{
	$sticky = get_option( 'sticky_posts' );

	if ( is_front_page() && is_home() ) {
		// Default homepage
	} elseif ( is_front_page() ) {
		// static homepage
	} elseif ( is_home() ) {
		$query->set( 'post__not_in', $sticky );
	} else {
		//everyting else
	}

}

// Add options page
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' 	=> 'Esticom Theme Settings',
        'menu_title'	=> 'Esticom Theme Settings',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));
}

// Enqueue styles
function theme_scripts() {
	
	// UI Kit Stylesheet
	wp_enqueue_style( 'esticom-uikit-stylesheet', get_template_directory_uri() . '/css/uikit.css', array(), '20160104' );
	
	// Scripts
	wp_enqueue_script( 'esticom-uikit-main-script', get_template_directory_uri() . '/js/uikit.min.js', array( 'jquery' ), '20160104', true );

	// Grid
	wp_enqueue_script( 'esticom-uikit-grid-script', get_template_directory_uri() . '/js/components/grid.min.js', array( 'jquery' ), '20160104', true );

	// Slideshow
	wp_enqueue_style( 'esticom-slider', get_template_directory_uri() . '/css/components/slider.min.css', array(), '1.0' );
	wp_enqueue_script( 'esticom-slider-script', get_template_directory_uri() . '/js/components/slider.js', array( 'jquery' ), '20160104', true );

	// Slideshow
	wp_enqueue_style( 'esticom-slideshow', get_template_directory_uri() . '/css/components/slideshow.min.css', array(), '1.0' );
	wp_enqueue_script( 'esticom-slideshow-script', get_template_directory_uri() . '/js/components/slideshow.min.js', array( 'jquery' ), '20160104', true );

	// Slidenav
	wp_enqueue_style( 'esticom-slidenav', get_template_directory_uri() . '/css/components/slidenav.min.css', array(), '1.0' );
	
	// Lightbox
	wp_enqueue_script( 'esticom-lightbox-script', get_template_directory_uri() . '/js/components/lightbox.min.js', array( 'jquery' ), '20160104', true );
	
	// Sticky
	wp_enqueue_script( 'esticom-sticky-script', get_template_directory_uri() . '/js/components/sticky.min.js', array( 'jquery' ), '20160104', true );
	
	// matchHeight
	wp_enqueue_script( 'esticom-matchHeight-script', get_template_directory_uri() . '/js/jquery.matchHeight.js', array( 'jquery' ), '20160104', true );

	// Main Stylesheet & Script
  // wp_enqueue_style( 'esticom-stylesheet', get_template_directory_uri() . '/style.css', array(), '1.0' );
	wp_enqueue_script( 'esticom-main-script', get_template_directory_uri() . '/js/main.js', array( 'jquery','esticom-uikit-main-script' ), '20160104', true );

	wp_enqueue_style('esticom-stylesheet', get_template_directory_uri().'/css/style.min.css', array(), '1.0');

}

add_action( 'wp_enqueue_scripts', 'theme_scripts' );

// Register custom post types
add_action( 'init', 'cptui_register_my_cpts' );
function cptui_register_my_cpts() {
	
	register_post_type( 'testimonials',
	// CPT Options
		array(
			'labels' => array(
				'name' => 'Testimonials',
				'singular_name' => 'Testimonial'
			),
			'description' => '',
			'public' => true,
			'show_ui' => true,
			'has_archive' => false,
			'show_in_menu' => true,
			'exclude_from_search' => false,
			'capability_type' => 'post',
			'map_meta_cap' => true,
            'menu_icon'   => 'dashicons-testimonial',
			'hierarchical' => false,
			'rewrite' => array( 'slug' => 'testimonials', 'with_front' => true ),
			'query_var' => true,
			'supports' => array( 'title', 'editor', 'revisions', 'thumbnail' )
		)
	);

	register_post_type( 'questions',
	// CPT Options
		array(
			'labels' => array(
				'name' => 'Questions',
				'singular_name' => 'Question'
			),
			'description' => '',
			'public' => true,
			'show_ui' => true,
			'has_archive' => false,
			'show_in_menu' => true,
			'exclude_from_search' => false,
			'capability_type' => 'post',
			'map_meta_cap' => true,
            'menu_icon'   => 'dashicons-welcome-learn-more',
			'hierarchical' => false,
			'rewrite' => array( 'slug' => 'questions', 'with_front' => true ),
			'query_var' => true,
			'supports' => array( 'title', 'editor', 'revisions', 'thumbnail' )
		)
	);

// End of cptui_register_my_cpts()
}

// Register Taxonomies
add_action( 'init', 'create_taxonomies', 0 );
function create_taxonomies() {
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => 'Question Categories',
        'singular_name'     => 'Question Category',
        'search_items'      => 'Search Question Categories',
        'all_items'         => 'All Question Categories',
        'parent_item'       => 'Parent Question Category',
        'parent_item_colon' => 'Parent Question Category:',
        'edit_item'         => 'Edit Question Category',
        'update_item'       => 'Update Question Category',
        'add_new_item'      => 'Add New Question Category',
        'new_item_name'     => 'New Question Category Name',
        'menu_name'         => 'Question Categories'
    );
    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'question' ),
    );
    register_taxonomy( 'question_category', array('questions'), $args );
}

// Page Slug Body Class
function add_slug_body_class( $classes ) {
    global $post;
    if ( isset( $post ) ) {
        $classes[] = $post->post_type . '-' . $post->post_name;
	}
    return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

// Featured Image
if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
}

// Custom excerpt length
function excerpt($limit) {
    return wp_trim_words(get_the_content(), $limit);
}

// This theme uses wp_nav_menu() in two locations.
register_nav_menus( array(
    'primary'   => __( 'Main Nav' ),
) );

// Register sidebar(s)
add_action( 'widgets_init', 'esticom_widgets_init' );
function esticom_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Footer Sidebar', 'esticom' ),
        'id' => 'footer-sidebar',
        'description' => __( 'Footer Sidebar', 'esticom' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
    ) );
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'esticom' ),
        'id' => 'main-sidebar',
        'description' => __( 'Main Sidebar', 'esticom' ),
        'before_widget' => '<li id="%1$s" class="widget sidebar-box %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
    ) );
}

/**
 * Add HTML5 theme support.
 */
 function wpdocs_after_setup_theme() {
    add_theme_support( 'html5', array( 'search-form' ) );
}
add_action( 'after_setup_theme', 'wpdocs_after_setup_theme' );

// Image sizes
add_image_size( 'post-image', 685, 260, true );
add_image_size( 'pagetitle-image', 1920, 255, true );

// Post ID from slug (needed for few page headers on single custom post type templates)
function get_id_by_slug($page_slug) {
	$page = get_page_by_path($page_slug);
	if ($page) {
		return $page->ID;
	} else {
		return null;
	}
}

// Custom admin login logo
function custom_login_logo() {
 echo '<style type="text/css">
 h1 a { background-image: url('.get_bloginfo('template_directory').'/img/logo.svg) !important; background-size: auto !important; width: auto !important; }
 </style>';
}
add_action('login_head', 'custom_login_logo');


// Custom Walker for header navigation
class UIKit_Menu_Walker extends Walker {

	/**
	 * Database fields to use.
	 *
	 * @see Walker::$db_fields
	 * @since 3.0.0
	 * @todo Decouple this.
	 * @var array
	 */
	public $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

	/**
	 * Starts the list before the elements are added.
	 *
	 * @see Walker::start_lvl()
	 *
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 */
    
    private $curItem;
    
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
        $title = $this->curItem;
		$output .= "\n$indent<div class=\"uk-dropdown uk-dropdown-navbar\"><ul class=\"uk-nav uk-nav-navbar\">\n";
	}

	/**
	 * Ends the list of after the elements are added.
	 *
	 * @see Walker::end_lvl()
	 *
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 */
	public function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul></div>\n";
	}

	/**
	 * Start the element output.
	 *
	 * @see Walker::start_el()
	 *
	 * @since 3.0.0
	 * @since 4.4.0 'nav_menu_item_args' filter was added.
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item   Menu item data object.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 * @param int    $id     Current item ID.
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        $this->curItem = $item;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

		/**
		 * Filter the arguments for a single nav menu item.
		 *
		 * @since 4.4.0
		 *
		 * @param array  $args  An array of arguments.
		 * @param object $item  Menu item data object.
		 * @param int    $depth Depth of menu item. Used for padding.
		 */
		$args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );
        
        /* If this item has a dropdown menu, add the 'dropdown' class for Bootstrap */
        if ($item->hasChildren) {
            $classes[] = 'uk-parent';
        }

		/**
		 * Filter the CSS class(es) applied to a menu item's list item element.
		 *
		 * @since 3.0.0
		 * @since 4.1.0 The `$depth` parameter was added.
		 *
		 * @param array  $classes The CSS classes that are applied to the menu item's `<li>` element.
		 * @param object $item    The current menu item.
		 * @param array  $args    An array of {@see wp_nav_menu()} arguments.
		 * @param int    $depth   Depth of menu item. Used for padding.
		 */
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

		/**
		 * Filter the ID applied to a menu item's list item element.
		 *
		 * @since 3.0.1
		 * @since 4.1.0 The `$depth` parameter was added.
		 *
		 * @param string $menu_id The ID that is applied to the menu item's `<li>` element.
		 * @param object $item    The current menu item.
		 * @param array  $args    An array of {@see wp_nav_menu()} arguments.
		 * @param int    $depth   Depth of menu item. Used for padding.
		 */
		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

		
        if ($item->hasChildren && $depth == 0) {
            $output .= $indent . '<li' . $id . $class_names .' data-uk-dropdown="{remaintime:300}" aria-haspopup="true" aria-expanded="false">';
        } else {
            $output .= $indent . '<li' . $id . $class_names .'>';
        }

		$atts = array();
		$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
		$atts['target'] = ! empty( $item->target )     ? $item->target     : '';
		$atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
		$atts['href']   = ! empty( $item->url )        ? $item->url        : '';

		/**
		 * Filter the HTML attributes applied to a menu item's anchor element.
		 *
		 * @since 3.6.0
		 * @since 4.1.0 The `$depth` parameter was added.
		 *
		 * @param array $atts {
		 *     The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
		 *
		 *     @type string $title  Title attribute.
		 *     @type string $target Target attribute.
		 *     @type string $rel    The rel attribute.
		 *     @type string $href   The href attribute.
		 * }
		 * @param object $item  The current menu item.
		 * @param array  $args  An array of {@see wp_nav_menu()} arguments.
		 * @param int    $depth Depth of menu item. Used for padding.
		 */
		$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

		$attributes = '';
		foreach ( $atts as $attr => $value ) {
			if ( ! empty( $value ) ) {
				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}

		/** This filter is documented in wp-includes/post-template.php */
		$title = apply_filters( 'the_title', $item->title, $item->ID );

		/**
		 * Filter a menu item's title.
		 *
		 * @since 4.4.0
		 *
		 * @param string $title The menu item's title.
		 * @param object $item  The current menu item.
		 * @param array  $args  An array of {@see wp_nav_menu()} arguments.
		 * @param int    $depth Depth of menu item. Used for padding.
		 */
		$title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . $title . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;

		/**
		 * Filter a menu item's starting output.
		 *
		 * The menu item's starting output only includes `$args->before`, the opening `<a>`,
		 * the menu item's title, the closing `</a>`, and `$args->after`. Currently, there is
		 * no filter for modifying the opening and closing `<li>` for a menu item.
		 *
		 * @since 3.0.0
		 *
		 * @param string $item_output The menu item's starting HTML output.
		 * @param object $item        Menu item data object.
		 * @param int    $depth       Depth of menu item. Used for padding.
		 * @param array  $args        An array of {@see wp_nav_menu()} arguments.
		 */
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	/**
	 * Ends the element output, if needed.
	 *
	 * @see Walker::end_el()
	 *
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item   Page data object. Not used.
	 * @param int    $depth  Depth of page. Not Used.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 */
	public function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= '</li>';
	}
    
    function display_element ($element, &$children_elements, $max_depth, $depth = 0, $args, &$output)
{
        // check whether this item has children, and set $item->hasChildren accordingly
        $element->hasChildren = isset($children_elements[$element->ID]) && !empty($children_elements[$element->ID]);

        // continue with normal behavior
        return parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    } 

}

/**
 * Improves the caption shortcode with HTML5 figure & figcaption; microdata & wai-aria attributes
 * 
 * @param  string $val     Empty
 * @param  array  $attr    Shortcode attributes
 * @param  string $content Shortcode content
 * @return string          Shortcode output
 */


?>
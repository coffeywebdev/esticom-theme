<?php
/**
 * Template for Tradig pages
 *
 * Template Name: Trading page
 *
 * @package WordPress
 * @subpackage Esticom
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

get_header(); ?>

    <?php
    // Start the loop.
    while ( have_posts() ) : the_post(); ?>

        <?php $heroBgImage = get_field('hero_image'); ?>
        <div id="hero" style="background:url(<?php echo $heroBgImage; ?>) top center no-repeat; background-size: cover;">
            <div class="uk-container uk-container-center">
                <div class="hero-intro">
                    <h1><?php the_title(); ?></h1>
                    <h2><?php the_field('small_page_title'); ?></h2>
                    <?php $form = get_field("hero_form"); ?>
                    <?php echo do_shortcode($form); ?>
                </div> <!-- Hero Intro -->
            </div>
        </div> <!-- Hero -->

        <section id="our-app">
            <div class="uk-container-center">
                <div class="uk-grid">
                    <div class="uk-width-medium-1-2">                        
                        <img src="<?php the_field('our_app_image'); ?>" />
                    </div>
                    <div class="uk-width-medium-1-2">
                        <div class="container-half">
                            <h5 class="title-span"><?php the_field('our_app_small_title'); ?></h5>
                            <h1><?php the_field('our_app_title'); ?></h1>
                            <?php the_field('our_app_content'); ?>
                        </div>
                    </div>
                </div>
            </div> <!-- UK Container -->
        </section> <!-- Our App -->

        <section id="trading-features" class="title-center uk-text-center">
            <div class="uk-container uk-container-center">
                <h5 class="title-span uk-text-center"><?php the_field('features_small_title'); ?></h5>
                <h1 class="uk-text-center"><?php the_field('features_title'); ?></h1>
                <?php the_field('features_content'); ?>
                <?php if( have_rows('features_list') ): ?>
                    <div class="features-list">
                    <?php while ( have_rows('features_list') ) : the_row(); ?>                
                        <div class="feature uk-text-center uk-width-medium-3-10">
                            <?php the_sub_field('icon'); ?>
                            <h3><?php the_sub_field('title'); ?></h3>                            
                        </div>                
                    <?php endwhile; ?>
                    </div> <!-- Box -->
                <?php endif; ?>
            </div> <!-- UK Container -->
        </section> <!-- Trading Features -->

        <section id="get-started" style="background:url(<?php the_field('get_started_background'); ?>) top center no-repeat; background-size: cover;">
            <div class="uk-container uk-container-center uk-text-center">
                <h2><?php the_field('get_started_title'); ?></h2>
                <p><?php the_field('get_started_text'); ?></p>
                <a href="#hero" class="uk-button uk-button-large" data-uk-smooth-scroll>Get Started for Free</a>
            </div> <!-- UK Container -->
        </section> <!-- Our App -->

    <?php // End the loop.
    endwhile;
    ?>
    
<?php get_footer(); ?>
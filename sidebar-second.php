<?php
/**
 * Default Sidebar
 *
 * @package WordPress
 * @subpackage Esticom
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}
?>
<ul>
    <?php if ( ! dynamic_sidebar( 'main-sidebar' ) ) : ?>
        <?php dynamic_sidebar( 'main-sidebar' ); ?>
    <?php endif; // end sidebar widget area ?>
</ul>
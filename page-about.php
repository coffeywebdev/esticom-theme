<?php
/**
 * Template for About page
 *
 * Template Name: About page
 *
 * @package WordPress
 * @subpackage Esticom
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

get_header(); ?>

    <?php
    // Start the loop.
    while ( have_posts() ) : the_post(); ?>

    <?php get_template_part( 'pagetitle' ); ?>

    <?php $topImage = get_field('top_section_image'); ?>
    <div class="content uk-container uk-container-center">
        <div class="uk-grid">
            <div class="uk-width-medium-2-3 uk-margin-large-top">
                <?php the_field('top_section_content'); ?>
            </div>
            <div class="uk-width-medium-1-3 uk-margin-large-top">
                <img src="<?php echo $topImage; ?>" />
            </div>
        </div>
    </div>

    <?php $contentImg = get_field('content_image'); ?>
    <div id="content">
        <div class="container">
            <div class="uk-grid">
                <div class="uk-width-large-1-3">
                    <img src="<?php echo $contentImg; ?>" />
                </div>
                <div class="uk-width-large-2-3 uk-margin-large">
                    <?php the_content(); ?>
                </div>
            </div>
        </div> <!-- UK Container -->
    </div> <!-- Content -->

    <?php $more = get_field('learn_more_bg_image'); ?>
    <div id="learn-more" style="background:url(<?php echo $more; ?>) no-repeat top center; background-size: cover;">
        <div class="uk-container uk-container-center">
            <div class="container-half">
                <h2><?php the_field('learn_more_title'); ?></h2>
                <a href="<?php the_field('learn_more_button_link'); ?>" class="uk-button uk-button-success"><?php the_field('learn_more_button'); ?></a>
            </div>
        </div> <!-- UK Container -->
    </div> <!-- Learn More -->

    <?php // End the loop.
    endwhile;
    ?>
    
<?php get_footer(); ?>
jQuery(document).ready(function($) {

        $('.app-box h3').matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });

        $('.app-box p').matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });

        $('.feature-box').matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });

        $('.feature-box h3').matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });

        $('.footer-box').matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });

        $('.bucket').matchHeight({
            byRow: false,
            property: 'height',
            target: null,
            remove: false
        });

        $('#button-click').on('click', function(event) {        
             $('#form-bottom').toggle('show');
             $(this).toggle('hide');
        });


});
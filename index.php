<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Esticom
 */
get_header();
	$sticky = get_option( 'sticky_posts' );
	$sticky_query = new WP_Query( 'p=' . $sticky[0] );
  $first_post = $sticky_query->posts[0];
?>

<div class="content">
  <div class="uk-container uk-container-center">

      <div class="uk-grid">
        <div class="post-container uk-width-medium-7-10 uk-width-1-1 <?php echo ($sticky) ? 'has-sticky-post' : 'no-sticky-post' ; ?>">


          <?php
	          if ( !is_paged() && $sticky ):
          ?>
          <div class="jumbo-post-wrapper">
            <!-- big post -->
              <div class="jumbo-post" style="background-image: url(<?= wp_get_attachment_image_src(get_post_thumbnail_id($first_post->ID), 'full')[0]; ?>);">
                <a class="post-link" rel="bookmark" href="<?= get_the_permalink($first_post->ID) ?>" title="<?= $first_post->post_title ?>"><h3 class="post-title"><?= $first_post->post_title ?></h3></a>
                <div class="post-meta">
                  <span class="category"><?= the_category(',') ?></span> &nbsp; <span class="read-time">5 min read</span>
                </div>
              </div>
          </div>
          <?php endif; ?>


            <?php
              while ( have_posts() ) : the_post(); ?>
                <!-- little post -->
                <div class="post">

	                <?php
                    $logo = get_stylesheet_directory_uri() . '/img/favicon.png';
                    $thumbnail_img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'medium')[0];
                    if($thumbnail_img):
                  ?>
                    <div class="thumbnail" style="background-image: url('<?= $thumbnail_img; ?>');">
                    </div>
                  <?php else: ?>
                    <div class="thumbnail no-thumbnail" style="background-image: url('<?= $logo; ?>');">
                    </div>
                  <?php endif; ?>
                  <div class="post-meta">
                    <a class="post-link" rel="bookmark" title="<?= get_the_title() ?>" href="<?= get_the_permalink() ?>">
                      <h3 class="post-title"><?php the_title(); ?></h3>
                    </a>
                    <span class="post-category"><?= the_category(',') ?></span> | <span class="read-time"><?= esticom_read_time() ?> min read</span>
                  </div>
                </div>
            <?php endwhile; ?>
            <?php wp_pagenavi(); ?>
        </div>
        <div class="uk-width-medium-3-10">
            <div class="sidebar">
                <?php get_sidebar( 'second' ); ?>
            </div>
        </div> <!-- Sidebar -->
      </div><!-- UK Grid -->

  </div> <!-- UK Container -->
</div> <!-- Content -->

<script>
  jQuery(document).ready(function($){
    $('body').on('click','.jumbo-post,.post', function(event){
      if(!$(event.target).is('a')) {
        window.location = $(this).find(".post-link").attr("href");
        return false;
      }
    });
  });

</script>
<?php get_footer(); ?>
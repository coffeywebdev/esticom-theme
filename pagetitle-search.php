<?php
/**
 * The template for displaying page titles
 *
 * @package WordPress
 * @subpackage Esticom
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

$id = get_id_by_slug('blog');
$image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'pagetitle-image'); ?>
<div id="page-header" class="uk-vertical-align" style="background:url(<?php echo $image[0]; ?>) center center no-repeat;">
    <div class="pagetitle uk-vertical-align-middle">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-xlarge-1-1">
                    <p>Searching for</p>
                </div>
                <div class="uk-width-xlarge-1-1">
                    <h1><?php echo get_search_query(); ?></h1>   
                </div>
            </div> <!-- UK Grid -->
        </div>
    </div> <!-- Pagetitle -->
</div> <!-- Page Header -->
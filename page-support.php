<?php
/**
 * Template for Support page
 *
 * Template Name: Support page
 *
 * @package WordPress
 * @subpackage Esticom
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

get_header(); ?>

    <?php
    // Start the loop.
    while ( have_posts() ) : the_post(); ?>


    <div class="hero">
      <div class="pagetitle uk-vertical-align-middle">
        <div class="uk-container uk-container-center">
          <div class="uk-grid">
            <div class="uk-width-medium-3-10 uk-width-small-1-1">
              <h1><?php the_title(); ?></h1>
            </div>
            <div class="uk-width-medium-7-10 uk-width-small-1-1">
              <div class="contact-us">
                <span>Still have unanswered questions?</span>
                <a class="uk-button uk-button-large yellow-button" href="<?= site_url('/contact/'); ?>">Contact Us</a>
              </div>
            </div>
          </div> <!-- UK Grid -->
        </div>
      </div> <!-- Pagetitle -->
    </div>

    <div class="content">
        <div class="uk-container uk-container-center">
            <div class="uk-grid uk-grid-small">
                <div class="uk-width-medium-2-10">
                    <ul id="tips" class="uk-subnav">
                        <li data-uk-filter=""><a href="">All</a></li>
                        <?php 
                        $terms = get_terms('question_category');
                        if ( !empty( $terms ) && !is_wp_error( $terms ) ):
                            foreach ( $terms as $term ) { ?>
                                <li data-uk-filter="filter-<?php echo $term->slug; ?>"><a href=""><?php echo $term->name; ?></a></li>
                        <?php } endif; ?>
                    </ul>
                </div>
                <div class="uk-width-medium-8-10">
                    <div class="bucket-wrapper uk-grid-width-small-1-1 uk-grid-width-xlarge-1-3 tm-grid-heights" data-uk-grid="{controls: '#tips', gutter: 30}">
                        <?php $custom_terms = get_terms('question_category');
                        $custom_terms = array_reverse($custom_terms);
                        foreach($custom_terms as $custom_term) {
                            wp_reset_query();
                            $args = array('post_type' => 'questions','posts_per_page' => -1,'tax_query' => array(array('taxonomy' => 'question_category','field' => 'slug','terms' => $custom_term->slug)));
                            $loop = new WP_Query($args);
                            if($loop->have_posts()) {
                                while($loop->have_posts()) : $loop->the_post(); ?>
                                <div data-uk-filter="filter-<?php echo $custom_term->slug; ?>">
                                  <div class="bucket resource">

                                  <div class="term <?= ($custom_term->name==="FAQ's") ? 'faq' : $custom_term->name ; ?>"><?php echo $custom_term->name; ?></div>
                                    <?php if(get_field('video_link')) { ?>

                                        <a class="" href="https://www.youtube.com/watch?v=<?php the_field('video_link'); ?>" data-uk-lightbox>
                                          <?php $video_thumbnail = get_field('video_thumbnail'); ?>
                                          <?php if(isset($video_thumbnail)): ?>
                                            <div class="video-thumbnail">
                                              <img src="<?= $video_thumbnail; ?>" />
                                            </div>
                                          <?php endif; ?>

                                            <br />
                                          <div class="read-more">
                                            <h3><?php the_title(); ?></h3>
                                          </div>
                                        </a>

                                    <?php } else { ?>

                                        <br />
                                        <div class="read-more">
                                          <h3><?php the_title(); ?></h3>
                                          <a href="<?php the_permalink(); ?>" class="more">Read More</a>
                                        </div>

                                    <?php } ?>
                                  </div>
                                </div>
                                <?php endwhile;
                            }
                        } ?>
                    </div>
                </div>
              <div class="uk-width-large-1-1">
                <div class="contact-us">
                  <span>Still have unanswered questions?</span>
                  <a class="uk-button uk-button-large yellow-button" href="<?= site_url('/contact/'); ?>">Contact Us</a>
                </div>
              </div>
            </div>
        </div> <!-- UK Container -->
    </div> <!-- Content -->

    <?php // End the loop.
    endwhile;
    ?>
    
<?php get_footer(); ?>
<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Esticom
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}   

get_header();

?>


<?php
  // Start the loop.
  while ( have_posts() ) : the_post();

    $id = get_the_ID();
    $title = get_the_title($id);
    $image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'full');
    $author_ID = get_the_author_meta('ID');
    $author_email = (get_the_author_meta('user_email') === 'rhock@esticom.com') ? '	clee@esticom.com' : get_the_author_meta('user_email');

    $avatar_img_src = get_avatar_url($author_email);
    $linkedin = get_user_meta($author_ID,'sabox_social_links',true)['linkedin'];

    $fname = get_the_author_meta('first_name');
    $lname = get_the_author_meta('last_name');
    $full_name = '';

    if( empty($fname)){
      $full_name = $lname;
    } elseif( empty( $lname )){
      $full_name = $fname;
    } else {
      //both first name and last name are present
      $full_name = "{$fname} {$lname}";
    }

  ?>
  <?php
    if($image[0]):
  ?>
    <div id="page-header" class="uk-vertical-align" style="background:url(<?php echo $image[0]; ?>) center center no-repeat;"></div>
  <?php
    endif;
  ?>

      <div class="content">

        <div class="uk-container uk-container-center">
          <div class="uk-grid skinny">

              <div class="uk-width-medium-7-10">
                <!-- Post Title -->
                <h1 class="blog-title"><?= $title ?></h1>
                <!-- Post Meta -->
                <div class="post-meta">
                  <div class="author">
                    <div class="avatar">
                      <img alt="<?= $full_name ?>" src="<?= $avatar_img_src ?>"/>
                    </div>
                    <div class="author-info">
                      <span class="written-by"><?= $full_name ?></span>
                      <span class="post-date"><?php the_date('F jS, Y'); ?></span>
                      <span class="post-category"><i>Published In:</i> <?php the_category(', '); ?></span>
                      <span class="linkedin">
                        <a href="<?= $linkedin ?>" class="link" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#555555" d="M0 0v24h24v-24h-24zm8 19h-3v-11h3v11zm-1.5-12.268c-.966 0-1.75-.79-1.75-1.764s.784-1.764 1.75-1.764 1.75.79 1.75 1.764-.783 1.764-1.75 1.764zm13.5 12.268h-3v-5.604c0-3.368-4-3.113-4 0v5.604h-3v-11h3v1.765c1.397-2.586 7-2.777 7 2.476v6.759z"/></svg> LinkedIn</a>
                      </span>
                    </div>
                  </div>
                </div>
                <!-- Post -->
                <div class="post post-single">
                  <?php the_content(); ?>
                </div>
              </div>

              <div class="uk-width-medium-3-10">
                <div class="sidebar">
                  <div class="uk-sticky-placeholder" style="height: 581px; margin: 0px 0px 50px;">
                    <div class="sidebar-box" data-uk-sticky="{boundary: '#related-posts'}" style="margin: 0px;">
	                    <?php
		                    $ad_image = get_field('sidebar_image', 'option');
		                    $ad_link = get_field('sidebar_image_link', 'option');
	                    ?>
                      <a href="<?= $ad_link; ?>">
	                      <img src="<?= $ad_image['url']; ?>"/>
                      </a>
                    </div>
                  </div>
                </div> <!-- Sidebar -->
              </div>

            </div>
          </div> <!-- UK Grid -->

      </div>

        <?php

            $args = array('category_name'=>get_the_category());
            $related_posts = get_posts($args);
            $post1_ID = $related_posts[0]->ID;
            $post2_ID = $related_posts[1]->ID;
            $post3_ID = $related_posts[2]->ID;
	          $logo = get_stylesheet_directory_uri() . '/img/favicon.png';
          ?>

      <div id="related-posts" class="uk-container uk-container-center">

        <div class="uk-grid">
          <h4  class="related-posts-title">Related Posts</h4>
        </div>

        <div class="uk-grid related-posts">
          <?php if($post1_ID): ?>
          <div class="col-4 post">
            <div class="uk-card uk-card-default uk-card-body">
              <a href="<?= get_the_permalink($post1_ID) ?>" rel="bookmark" title="<?= get_the_title($post1_ID) ?>">
                <div class="<?php echo (get_the_post_thumbnail_url($post1_ID)) ? 'image' : 'no-thumbnail';  ?>" style="background-image:url(<?php echo (get_the_post_thumbnail_url($post1_ID)) ? get_the_post_thumbnail_url($post1_ID) : $logo;  ?>);"></div>
                <div class="title">
                  <?= get_the_title($post1_ID) ?>
                  <span class="date"><?= get_the_date('F jS, Y',$post1_ID) ?></span>
                </div>
              </a>
            </div>
          </div>
          <?php endif; ?>

          <?php if($post2_ID): ?>
          <div class="col-4 post">
            <div class="uk-card uk-card-default uk-card-body">
              <a href="<?= get_the_permalink($post2_ID) ?>" rel="bookmark" title="<?= get_the_title($post2_ID) ?>">
                <div class="<?php echo (get_the_post_thumbnail_url($post2_ID)) ? 'image' : 'no-thumbnail';  ?>" style="background-image:url(<?php echo (get_the_post_thumbnail_url($post2_ID)) ? get_the_post_thumbnail_url($post2_ID) : $logo;  ?>);"></div>
                <div class="title">
                  <?= get_the_title($post2_ID) ?>
                  <span class="date"><?= get_the_date('F jS, Y',$post2_ID) ?></span>
                </div>
              </a>
            </div>
          </div>
          <?php endif; ?>

          <?php if($post3_ID): ?>
          <div class="col-4 post">
            <div class="uk-card uk-card-default uk-card-body">
              <a href="<?= get_the_permalink($post3_ID) ?>" rel="bookmark" title="<?= get_the_title($post3_ID) ?>">
                <div class="image <?php echo (get_the_post_thumbnail_url($post3_ID)) ? '' : 'no-thumbnail';  ?>" style="background-image:url(<?php echo (get_the_post_thumbnail_url($post3_ID)) ? get_the_post_thumbnail_url($post3_ID) : $logo;  ?>);"></div>
                <div class="title">
                  <?= get_the_title($post3_ID) ?>
                  <span class="date"><?= get_the_date('F jS, Y',$post3_ID) ?></span>
                </div>
              </a>
            </div>
          </div>
          <?php endif; ?>

        </div>

      </div> <!-- UK Container -->


  <?php // End the loop.
    endwhile;
  ?>
<?php get_footer(); ?>
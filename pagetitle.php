<?php
/**
 * The template for displaying page titles
 *
 * @package WordPress
 * @subpackage Esticom
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

$image = wp_get_attachment_image_src(get_post_thumbnail_id($imageID), 'pagetitle-image'); ?>
<div id="page-header" class="" style="background:url(<?php echo $image[0]; ?>) center center no-repeat;">
    <div class="pagetitle-bg uk-vertical-align">
        <div class="pagetitle uk-vertical-align-middle">
            <div class="uk-container uk-container-center">
                <div class="uk-grid">
                    <div class="uk-width-xlarge-1-1">
                        <h1><?php the_title(); ?></h1>
                    </div>
                    <?php if(get_field('small_page_title')) { ?>
                    <div class="uk-width-xlarge-1-1">
                        <p><?php the_field('small_page_title'); ?></p>
                    </div>
                    <?php } ?>
                </div> <!-- UK Grid -->
            </div>
        </div> <!-- Pagetitle -->
    </div>
</div> <!-- Page Header -->
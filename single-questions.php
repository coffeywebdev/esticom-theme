<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Esticom
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}   

get_header(); ?>


  <div class="header">
    <div class="pagetitle uk-vertical-align-middle">
      <div class="uk-container uk-container-center">
        <div class="uk-grid">
          <div class="uk-width-xlarge-1-1">
            <span class="pagetitle">Resources</span>
          </div>
        </div> <!-- UK Grid -->
      </div>
    </div> <!-- Pagetitle -->
  </div> <!-- Page Header -->

  <?php
    // Start the loop.
    while ( have_posts() ) : the_post();
  ?>
      <div class="content">
          <div class="uk-container uk-container-center">
              <div class="post post-single">
                  <span class="date"><?php echo get_the_term_list(get_the_ID(), 'question_category'); ?></span>
                  <h1><?php the_title(); ?></h1>
                  <?php the_content(); ?>
              </div> <!-- Post -->
          </div> <!-- UK Container -->
      </div> <!-- Content -->
  <?php // End the loop.
    endwhile;
  ?>
<?php get_footer(); ?>
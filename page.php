<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Esticom
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

get_header(); ?>

    <?php
    // Start the loop.
    while ( have_posts() ) : the_post(); ?>

    <?php get_template_part( 'pagetitle' ); ?>

    <div class="content">
        <div class="uk-container uk-container-center">
            <?php the_content(); ?>
        </div> <!-- UK Container -->
    </div> <!-- Content -->

    <?php // End the loop.
    endwhile;
    ?>
    
<?php get_footer(); ?>
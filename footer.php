<?php
/**
 * The template for displaying the footer
 *
 * @package WordPress
 * @subpackage Esticom
 */
?>
    
    <div id="footer"> 
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-medium-1-2">
                    <?php $footerLogo = get_field('footer_image', 'option'); ?>
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo $footerLogo; ?>" /></a>
                    <p><?php the_field('footer_text', 'option'); ?></p>
                </div>
                <div class="uk-width-medium-1-2">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </div> <!-- Footer -->

    <div id="copyright" class="uk-container uk-container-center uk-text-center">
        <p class="uk-text-right">Copyright &copy; <?php echo date("Y"); ?> Esticom Inc. All Rights Reserved</p>
    </div> <!-- Copyright -->

    <?php wp_footer(); ?>

	</body>
</html>
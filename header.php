<?php
/**
 * The template for displaying the header
 *
 * @package WordPress
 * @subpackage Esticom
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"> 
        
        <!-- Favicons -->  
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/img/favicons/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/img/favicons/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/img/favicons/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/img/favicons/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/img/favicons/apple-touch-icon-60x60.png" />
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/img/favicons/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/img/favicons/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/img/favicons/apple-touch-icon-152x152.png" />
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/img/favicons/favicon-196x196.png" sizes="196x196" />
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/img/favicons/favicon-96x96.png" sizes="96x96" />
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/img/favicons/favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/img/favicons/favicon-16x16.png" sizes="16x16" />
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/img/favicons/favicon-128.png" sizes="128x128" />
        <meta name="application-name" content="Electrical Estimating Software - Structured Cabling Estimating Software - Esticom"/>
        <meta name="msapplication-TileColor" content="#FFFFFF" />
        <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/img/favicons/mstile-144x144.png" />
        <meta name="msapplication-square70x70logo" content="<?php echo get_template_directory_uri(); ?>/img/favicons/mstile-70x70.png" />
        <meta name="msapplication-square150x150logo" content="<?php echo get_template_directory_uri(); ?>/img/favicons/mstile-150x150.png" />
        <meta name="msapplication-wide310x150logo" content="<?php echo get_template_directory_uri(); ?>/img/favicons/mstile-310x150.png" />
        <meta name="msapplication-square310x310logo" content="<?php echo get_template_directory_uri(); ?>/img/favicons/mstile-310x310.png" />
        
        <title><?php wp_title(); ?></title>
        
        <script>(function(){document.documentElement.className='js'})();</script>
        <script async src="https://cdn.ampproject.org/v0.js"></script>
        <?php wp_head(); ?>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&Roboto+Condensed:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300i,400" rel="stylesheet">
    </head>
    <body <?php body_class(); ?>>
        <div id="header">
            <div class="uk-container uk-container-center">
                <nav class="uk-navbar">
                    <div class="uk-grid">
                        <div class="uk-width-small-2-10 uk-width-1-10">
                            <?php $logo = get_field('logo', 'option'); ?>
                            <a class="uk-navbar-brand brand uk-visible-large" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo $logo['url']; ?>" title="<?php echo $logo['title']; ?>" alt="<?php echo $logo['alt']; ?>" /></a>
                            <a href="#offcanvas" class="uk-navbar-toggle" data-uk-offcanvas></a>
                        </div>
                        <div class="uk-width-small-8-10 uk-width-9-10">
                            <div class="uk-grid uk-grid-collapse">
                                <div class="uk-width-large-8-10 uk-width-6-10">
                                    <!-- Navigation -->
                                    <?php wp_nav_menu(array( 
                                        'theme_location'    => 'primary',
                                        'walker'            => new UIKit_Menu_Walker(),
                                        'items_wrap'        => '<ul class="uk-navbar-nav uk-visible-large">%3$s</ul>',
                                        'container'         => ''
                                    )); ?>
                                    <?php $logo = get_field('logo', 'option'); ?>
                                    <a class="uk-navbar-brand uk-navbar-center" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo $logo['url']; ?>" title="<?php echo $logo['title']; ?>" alt="<?php echo $logo['alt']; ?>" /></a>
                                </div>
                                <div class="uk-width-large-2-10 uk-width-4-10">
                                    <ul class="header-list">
                                        <li><a href="https://app.esticom.com/Account/Login" class="login">Login</a></li>
                                        <!-- <li><a href="#search" class="search" data-uk-modal="{center:true}"><img src="<?php echo get_template_directory_uri(); ?>/img/search.png" /></a></li> -->
                                    </ul>
                                    <!-- This is the modal -->
                                    <div id="search" class="uk-modal uk-modal-search">
                                        <div class="uk-modal-dialog">
                                            <a class="uk-modal-close uk-close"></a>
                                            <?php get_search_form(); ?>
                                        </div>
                                    </div> <!-- UK Modal -->
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div> <!-- Header -->
        
        <div id="offcanvas" class="uk-offcanvas">
            <div class="uk-offcanvas-bar">
                <div class="offcanvas-logo uk-text-center">
                    <?php $logo = get_field('logo', 'option'); ?>
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo $logo['url']; ?>" title="<?php echo $logo['title']; ?>" alt="<?php echo $logo['alt']; ?>" /></a>
                </div>
                <!-- Navigation -->
                <?php wp_nav_menu(array( 
                    'theme_location'    => 'primary',
                    'walker'            => new UIKit_Menu_Walker(),
                    'items_wrap'        => '<ul class="uk-nav uk-nav-offcanvas">%3$s</ul>',
                    'container'         => ''
                )); ?>
            </div>
        </div> <!-- Offcanvas -->
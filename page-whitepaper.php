<?php
/**
 * Template for Whitepaper page
 *
 * Template Name: Whitepaper page
 *
 * @package WordPress
 * @subpackage Esticom
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

get_header(); ?>

    <?php
    // Start the loop.
    while ( have_posts() ) : the_post(); ?>

    <div class="content">
        <div class="uk-container uk-container-center">
            <div class="content-box">
                <h5 class="title-span"><?php the_field('small_page_title'); ?></h5>
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>
                <div id="bottom">
                    <?php $title = get_field('bottom_title'); ?>
                    <?php if($title) { ?>
                        <h1><?php the_field('bottom_title'); ?></h1>
                    <?php } ?>
                    <div id="form-bottom">
                        <?php the_field('bottom_form'); ?>
                    </div>
                </div>
            </div> <!-- Content Box -->
        </div> <!-- UK Container -->
    </div> <!-- Content -->

    <?php // End the loop.
    endwhile;
    ?>
    
<?php get_footer(); ?>
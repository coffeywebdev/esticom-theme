<?php
/**
 * Template for Pricing page
 *
 * Template Name: Pricing page
 *
 * @package WordPress
 * @subpackage Esticom
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

get_header(); ?>

    <?php
    // Start the loop.
    while ( have_posts() ) : the_post(); ?>

    <?php get_template_part( 'pagetitle' ); ?>

    <div class="content" class="title-center uk-text-center">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <?php if( have_rows('plans') ): ?>
                <?php while ( have_rows('plans') ) : the_row(); ?>
                    <div class="uk-width-large-1-3">
                        <div class="plans">
                            <h2><?php the_sub_field('plan_title'); ?></h2>
                            <div class="price">
                                <p><span><?php the_sub_field('price'); ?></span><?php the_sub_field('price_per'); ?></p>
                                <?php if(get_sub_field('save')) { ?>
                                    <p><?php the_sub_field('save'); ?></p>
                                <?php } ?>
                            </div>
                            <ul class="pricing-list">
                                <?php if( have_rows('list') ): ?>
                                <?php while ( have_rows('list') ) : the_row(); ?>
                                    <li>
                                        <h3><?php the_sub_field('list_title'); ?></h3>
                                        <p><?php the_sub_field('list_text'); ?></p>
                                    </li>
                                <?php endwhile; ?>
                                <?php endif; ?>
                            </ul>
                            <a href="<?php the_sub_field('button_link'); ?>" class="uk-button uk-button-success"><?php the_sub_field('button_text'); ?></a>
                        </div> <!-- Plans -->
                    </div>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div> <!-- UK Container -->
    </div> <!-- Content -->

    <div class="uk-container uk-container-center uk-margin-large-bottom">
        <div id="bottom">
            <?php $title = get_field('bottom_title'); ?>
            <?php if($title) { ?>
                <h2><?php the_field('bottom_title'); ?></h2>
                <p><?php the_field('bottom_small_title'); ?></p>
            <?php } ?>
            <div id="form-bottom" class="uk-margin-large">
                <?php the_field('bottom_form'); ?>
            </div>
        </div>
    </div> <!-- UK Container -->

    <?php // End the loop.
    endwhile;
    ?>
    
<?php get_footer(); ?>
<?php
/**
 *
 * Template Name: eBook Landing Page
 *
 * @package WordPress
 * @subpackage Esticom
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

get_header(); ?>

  <?php
    // Start the loop.
    while ( have_posts() ) : the_post();
  ?>
  <header>
    <div class="breadcrumbs">
      <a class="breadcrumb" href="<?= site_url('support') ?>" title="Resources" rel="bookmark">Resources</a>
        <span class="raquo">&raquo;</span>
          <a href="<?= get_the_permalink() ?>" title="<?= get_the_title() ?>" class="breadcrumb" rel="bookmark">Ebook</a>
    </div>
    <h1>Inside Real-World Electrical Estimation</h1>
  </header>
  <main>

    <div class="uk-container uk-container-center">


        <div class="uk-grid" data-uk-grid-match>

          <!-- image / left -->
          <div class="uk-width-large-1-2">
            <div class="cover-image--wrapper">
              <img src="/wp-content/themes/esticom/img/ebook-cover-image.png"/>
            </div>
          </div>

          <!-- form / call to action -->
          <div class="uk-width-large-1-2">
            <div class="form-wrapper">
              <h2 class="form-headline">Get Your Free Copy Now!</h2>
              <form id="form">

                <div class="uk-grid">
                  <div class="uk-width-medium-1-2 my-1">
                    <input name="firstname" placeholder="First Name*" required>
                  </div>

                  <div class="uk-width-medium-1-2 my-1">
                    <input name="lastname" placeholder="Last Name*" required>
                  </div>

                  <div class="uk-width-1-1 my-1">
                    <input name="email" placeholder="Work Email*" type="email" required>
                  </div>

                  <div class="uk-width-1-1 my-1">
                    <input name="phone" placeholder="Phone Number*" type="tel" pattern="\d{3}[\-]\d{3}[\-]\d{4}" required>
                  </div>

                  <div class="uk-width-1-1">
                    <input type="submit" value="Download Free">
                  </div>

                  <div class="form-errors uk-width-1-1">

                  </div>

                </div>

              </form>

            </div>
          </div>
          <div class="uk-width-1-1">
            <h2>Get All 8 Chapters of Esticom's Inside Real-World Electrical Estimating in 1 eBook!</h2>
            <div class="uk-width-large-1-2">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab adipisci facilis illo nisi quae quaerat quia reprehenderit veritatis? Accusantium consectetur, eligendi eveniet fuga hic ipsam nihil nobis officia pariatur vero?</p>
              <div class="divider"></div>
              <p><b>In this electrical estimating guide we'll cover 8 key topics, including:</b></p>
              <ul>
                <li>How to Navigate the Electrical Bidding Process</li>
                <li>Reviewing Electrical Drawings and Specifications</li>
                <li>How to Perform a Quantity Takeoff for Electrical</li>
                <li>Developing Your Electrical Cost Estimate</li>
                <li>The Imperative: Adjusting for Profit, Overhead & Taxes</li>
                <li>Building a Winning Construction Proposal</li>
                <li>Types of Construction Contracts and the Change Order Process</li>
                <li>The Case for Electrical Estimating Software and Conclusion</li>
              </ul>
              <div class="divider"></div>
              <p>If you're just getting started as an electrical contractor and new to electrical estimating, it is our goal to provide you with a solid foundation to get you up and running. And if you're a seasoned electrical estimator, this guide may simply act as a refresher or you may even learn a few new things along the way.</p>
            </div>

          </div>

        </div>


    </div>


  </main>

  <?php // End the loop.
    endwhile;
  ?>

<?php get_footer(); ?>
<script>
  jQuery(function($){
    const $errors = $('.form-errors');
    $('#form').on('submit', function(event){
      event.preventDefault()
      $errors.html(''); // clear form errors
      let [firstname,lastname,email,phone] = $(this).serializeArray()


      let data = {
        fields: [
          {
            name: firstname.name,
            value: firstname.value
          },
          {
            name: lastname.name,
            value: lastname.value
          },
          {
            name: email.name,
            value: email.value
          },
          {
            name: phone.name,
            value: phone.value
          }
        ],
      }

      console.log(data)

      $.ajax({
        method: 'post',
        url: 'https://api.hsforms.com/submissions/v3/integration/submit/2819106/d2089c2c-4d5c-49e0-88a2-abd56c2404be',
        contentType: 'application/json',
        crossOrigin: true,
        dataType: 'json',
        data: JSON.stringify(data),
        success: function(data, status, xhr){
          console.log({data,status,xhr})
          const message = `
          <section class="download-ebook">
            <div class="uk-width-1-1">
              <p>Download your copy of <i>Inside Real-World Electrical Estimation</i> by clicking the button below!</p>
              <a href="/wp-content/uploads/Real%20World%20Electrical%20Estimating.pdf" class="download-button" target="_blank">Download eBook</a>
            </div>
          </section>
          `
          $('.form-headline').text('Thank you for downloading!')
          $('#form').html(message)

        },
        error: function(xhr, status, error){
          console.log({xhr,status,error})
          const jsonResponse = xhr.responseJSON
          const {errors} = jsonResponse
          errors.forEach(error => {
            if(error.errorType==="INVALID_EMAIL"){
              console.log('invalid email...')
              $('input[name=email').addClass('error')
              $errors.append(`<p>The e-mail address provided is invalid, please correct and try again.`)
            }
            if(error.errorType==="NUMBER_OUT_OF_RANGE"){
              console.log('number out of range...')
              $('input[name=phone').addClass('error')
              $errors.append(`<p>The phone number provided is invalid, please correct and try again.`)
            }
          })
        }
      })

    })

  })

</script>

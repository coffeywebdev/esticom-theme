<?php
/**
 * Template for Trade Pages
 *
 * Template Name: Trade Page
 *
 * @package WordPress
 * @subpackage Esticom
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

get_header(); ?>

    <?php
    // Start the loop.
    while ( have_posts() ) : the_post();
      $page_title = get_the_title();
      $hero = get_field('trade_hero');
      //$subhero = get_field('trade_subhero'); todo: remove, not used
      $features = get_field('features_fields');
      $features_list = get_field('features_list');
      $section_one = get_field('content_section_1');
      $section_two = get_field('content_section_2');
      $white_paper = get_field('white_paper');
      $callout = get_field('callout_fields')

    ?>

    <!-- HEADER -->
    <div class="trade-page-header" style="background:url('<?= $hero['background_image']; ?>');">

        <div class="pagetitle uk-vertical-align-middle">
          <div class="uk-container uk-container-center">
            <div class="uk-grid">
              <div class="uk-width-xlarge-1-2 headlines">
                <span class="small-title"><?= $hero['tagline']; ?></span>
                <h1 class="big-title"><?= $page_title; ?></h1>
                <?php if(isset($hero['buttons'])): ?>
                <div class="button-group uk-grid">
                  <div class="uk-width-xlarge-1-2 uk-width-small-1-2">
                    <a href="<?= $hero['buttons'][0]['url']; ?>" class="uk-button uk-button-large-custom yellow-button"><?= $hero['buttons'][0]['text']; ?></a>
                  </div>
                  <div class="uk-width-xlarge-1-2 uk-width-small-1-2">
                    <a href="<?= $hero['buttons'][1]['url']; ?>" class="uk-button uk-button-large-custom transparent-button"><?= $hero['buttons'][1]['text']; ?></a>
                  </div>
                </div>
                <div class="starts-at">
                  <span><?= $hero['note']; ?></span>
                </div>
                <?php endif; ?>

              </div>
              <div class="uk-width-xlarge-1-2 video">
                <?php if(isset($hero['youtube_id']) && isset($hero['call_to_action_image'])): ?>
                  <a href="https://www.youtube.com/watch?v=<?= $hero['youtube_id'] ?>" class="video-btn uk-overlay" data-uk-lightbox>
                    <img src="<?= $hero['call_to_action_image']; ?>" alt="<?= $page_title; ?>"/>
                  </a>
                <?php endif; ?>
              </div>
            </div> <!-- UK Grid -->
          </div>
        </div> <!-- Pagetitle -->

    </div> <!-- Page Header -->


    <!-- FEATURES -->
    <div class="features">
      <div class="uk-container uk-container-center">
        <div class="headlines">
          <span class="small-title">CLOUD-BASED | INSTANT ACCESS | NO DOWNLOAD | ZERO SETUP</span>
          <h2 class="big-title"><?= $features['headline']; ?></h2>
          <span class="tagline"><?= $features['tagline']; ?></span>
        </div>
        <div class="uk-grid">
          <?php
            foreach($features['features'] as $feature):
          ?>
            <div class="uk-width-medium-1-3 uk-width-small-1-1">
              <div class="feature">
                <img src="<?= $feature['icon']; ?>" alt="<?= $feature['title']; ?>"/>
                <span class="feature-title"><?= $feature['title']; ?></span>
                <div>
                  <?= $feature['description']; ?>
                </div>
              </div>
            </div>
          <?php
            endforeach;
          ?>
        </div>
      </div>
    </div>

    <div class="features-list background-light-gray">
      <div class="uk-container uk-container-center">
        <div class="headlines">
          <span class="small-title"><?= $features_list['tagline']; ?></span>
          <h2 class="big-title"><?= $features_list['headline']; ?></h2>
        </div>
        <div class="uk-grid list-wrapper">
          <?php
            $feature_list_blocks = $features_list['features'];
            if(!empty($feature_list_blocks)):
              foreach ($feature_list_blocks as $block){
          ?>
          <div class="uk-width-large-1-3 uk-width-medium-1-2 uk-width-small-1-1 list-block">
            <h2><?= $block['title']; ?></h2>
              <ul>
              <?php
                foreach ($block['features'] as $feature){
                ?>
                  <li><?= $feature['feature']; ?></li>
              <?php
                }
              ?>
              </ul>
          </div>
          <?php
            }
            endif;
          ?>
        </div>
      </div>
    </div>

    <?php
      if($callout):
    ?>
    <div class="callout-1 background-blue">
      <div class="uk-container uk-container-center">
        <div class="uk-grid">
          <div class="uk-width-large-1-2 uk-width-small-1-1 headlines">
            <h2><?= $callout['headline']; ?></h2>
            <p><?= $callout['tagline']; ?></p>
          </div>
          <div class="uk-width-large-1-2 uk-width-small-1-1 buttons">
            <?php
              if($callout['buttons']):
                $counter = 1;
                foreach ($callout['buttons'] as $button):
                  $button_class = ($counter === 1) ? 'yellow-button' : 'transparent-button';
            ?>
                <a href="<?= $button['link']; ?>" class="uk-button uk-button-large-custom <?= $button_class ?>"><?= $button['text'] ?></a>
            <?php
                $counter++;
                endforeach;
              endif;
            ?>
          </div>
        </div>
      </div>
    </div>
    <?php
      endif;
    ?>

    <div class="trade-page-slider">
      <div class="headlines">
        <h2 class="big-title">Take a Closer Look at Esticom</h2>
      </div>

      <div class="uk-container uk-container-center">

        <div class="slider-wraper">
          <div class="uk-slidenav-position" data-uk-slider="">
            <div class="uk-slider-container">
              <ul class="uk-slider uk-grid-small uk-grid-width-medium-1-3">
                <?php
                  $slides = get_field('feature_slides');
                  $counter = 1;
                  if(!empty($slides)):
                    foreach($slides as $slide):
                      ?>
                      <li>
                        <div class="slide-wrap">
                          <a href="<?= $slide['image']; ?>" data-uk-lightbox="{group:'screenshots'}" title="<?= $slide['caption']; ?>">
                            <img src="<?= $slide['image']; ?>">
                            <span class="caption"><?= $slide['caption']; ?></span>
                          </a>
                        </div>
                      </li>
                      <?php
                      $counter++;
                    endforeach;
                  endif;
                ?>
              </ul>
            </div>
            <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slider-item="previous"></a>
            <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slider-item="next"></a>
          </div>
        </div>

      </div>

    </div>

      <!-- DIVIDER -->
    <div class="divider">
      <div class="uk-container uk-container-center">
        <hr>
      </div>
    </div>


    <!-- Content Section #1 -->
    <div class="content-section-1">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-large-1-2 uk-width-small-1-1">
                    <img src="<?= $section_one['image']; ?>" alt="<?= $section_one['headline']; ?>" />
                </div>
                <div class="uk-width-large-1-2 uk-width-small-1-1 headlines">
                    <h2 class="big-title"><?= $section_one['headline']; ?></h2>
                    <p><?= $section_one['copy']; ?></p>
                    <p class="button-wrapper">
                      <a href="<?= $section_one['button'][0]['url']; ?>" class="uk-button uk-button-large yellow-button">
                        <?= $section_one['button'][0]['text']; ?>
                      </a>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- Testimonials -->
    <div class="testimonial background-blue">
      <div class="uk-container uk-container-center uk-text-center">
<!--        <img alt="Testimonial" src="/wp-content/themes/esticom/img/trade-pages/quote-icon.svg"/>-->


          <div class="testimonial-wrapper" data-uk-slideshow="{autoplay:true}">

            <ul class="uk-slideshow">
              <?php
                $mypost = array( 'post_type' => 'testimonials', 'order' => 'ASC', 'posts_per_page' => -1 );
                $loop = new WP_Query( $mypost );
                while ( $loop->have_posts() ) : $loop->the_post();
                  $post_id = get_the_ID();
                  $avatar = get_field('avatar', $post_id);
                  $title = get_field('title', $post_id);
                ?>
                  <li class="testimonial-content">
                    <div class="avatar">
                      <img src="<?= $avatar; ?>"/>
                    </div>
                    <div class="testimonial">
                      <?php the_content(); ?>
                    </div>
                    <span class="author"><?= get_the_title(); ?></span>
                    <span class="title"><?= $title; ?></span>
                  </li>
                <?php endwhile; ?>
              <?php wp_reset_query(); ?>
            </ul> <!-- Slideshow -->

            <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
            <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>


          </div>



      </div>
    </div>

    <!-- Content Section #2 -->
    <div class="content-section-2">
        <div class="uk-container uk-container-center">
          <div class="uk-grid">
            <div class="uk-width-large-1-2">
              <h2 class="big-title"><?= $section_two['headline']; ?></h2>
              <p><?= $section_two['copy']; ?></p>
            </div>
            <div class="uk-width-large-1-2">
              <div class="section-three-img">
                <img src="<?= $section_two['image'] ?>" alt="<?= $section_two['headline']; ?>"/>
              </div>
              <div class="button-group uk-grid">
                <div class="uk-width-xlarge-1-2 uk-width-large-1-1 uk-width-medium-1-2">
                  <a href="<?= $section_two['buttons'][0]['url']; ?>" class="uk-button uk-button-large-custom yellow-button">
                    <?= $section_two['buttons'][0]['text']; ?>
                  </a>
                </div>
                <div class="uk-width-xlarge-1-2 uk-width-large-1-1 uk-width-medium-1-2">
                  <a href="<?= $section_two['buttons'][1]['url']; ?>" class="uk-button uk-button-large-custom blue-transparent-button ">
                    <?= $section_two['buttons'][1]['text']; ?>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
  </div>

  <!-- White Paper -->
  <div class="white-paper">
      <div class="uk-container uk-container-center">
          <div class="uk-grid uk-grid-large">
              <?php $paper_image = get_field('paper_image', 'option'); ?>

              <div class="uk-width-1-1 white-paper-container">
                <div class="image">
                  <img src="<?= $paper_image;?>" alt="<?= $white_paper['headline'] ?>"/>
                </div>
                <div class="flexbox">
                  <h2 class="headline"><?= $white_paper['headline'] ?></h2>
                  <div class="button-container">
                    <a href="<?= $white_paper['file'] ?>" class="uk-button uk-button-large-custom yellow-button" target="_blank">
                      <?= $white_paper['button_text'] ?>
                    </a>
                  </div>
                </div>


              </div>


          </div>
      </div>
  </div>

  <div class="pricing-section background-light-gray uk-text-center">
      <div class="uk-container uk-container-center">
          <h2 class="uk-text-center big-title"><?php the_field('pricing_title', 'option'); ?></h2>
          <?php if( have_rows('pricing', 'option') ): ?>
              <div class="uk-grid uk-grid-match price-features-list">
              <?php while ( have_rows('pricing', 'option') ) : the_row(); ?>
                  <div class="uk-text-center uk-width-large-1-3 uk-width-medium-1-1">
                    <div class="price-features">
                      <span class="h2"><?php the_sub_field('title', 'option'); ?></span>
                      <p><?php the_sub_field('text', 'option'); ?></p>
                    </div>
                  </div>
              <?php endwhile; ?>
              </div> <!-- Box -->
          <?php endif; ?>
          <a href="<?php the_field('pricing_button_link', 'option'); ?>" class="uk-button"><?php the_field('pricing_button', 'option'); ?></a>
      </div> <!-- UK Container -->
  </div> <!-- Trading Features -->

    <?php // End the loop.
    endwhile;
    ?>
    
<?php get_footer(); ?>